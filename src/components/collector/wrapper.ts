import * as Tripetto from 'tripetto-collector';
import { Component, Input } from '@angular/core';
import { TEMPLATE } from './template';

@Component({
  selector: 'app-collector-wrapper',
  template: `
      ${TEMPLATE}

      <mat-card *ngIf="type == ''" style="margin-bottom: 20px;">
        <mat-card-title *ngIf="name != ''">
          {{name}}
        </mat-card-title>
        <mat-card-content *ngIf="node.Props.Description">
          {{node.Props.Description}}
        </mat-card-content>
      </mat-card>
    `
})
export class WrapperComponent {
  @Input() node: Tripetto.IObservableNode<{}>;

  get name(): string {
    return this.node.Props.NameVisible ? this.node.Props.Name : '';
  }

  get type(): string {
    return this.node.Props.Block ? this.node.Props.Block.Type : '';
  }

  get props(): {} {
    const block = this.node.Block;

    if (!block) {
      throw new Error('The block is invalid!');
    }

    return block.OnRender(this.node.Instance, this.node.Observer);
  }
}
